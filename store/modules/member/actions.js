import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_MEMBER]: (store, payload) => {
        return axios.get(apiUrls.DO_MEMBER, payload)
    },
}
